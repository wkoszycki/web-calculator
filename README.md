web-calculator
================
Web calculator  based on rest API

Functionality:

1) Add/Deduct/Multiply/Divide

2) Square/Root

3) Support brackets ( / [ / {

4) Support displaying calculation history 

5) Calculate e^x integral* in asynchronous multi threading manner


Technologies:

Frontend
-html,css,angularJS

Backend
-CDI,JAX-RS,